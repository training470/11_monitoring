## Используемый стек
[Prometheus](http://localhost:9090/graph)  
[Node Exporter](http://localhost:9100/metrics)  
[Alert Manager](http://localhost:9093)  
[Grafana](http://localhost:3000/login) - admin/admin  
[Promtail](http://localhost:9080/targets)

## Настройки
1. [Добавить](/etc/prometheus/prometheus.yml:) метрики node_exporter в Prometheus

## Выполнение заданий
### Задание 1 
[Prometheus](http://localhost:9090/graph?g0.range_input=1h&g0.moment_input=2022-09-15%2015%3A31%3A12&g0.expr=node_cpu_seconds_total%7Bmode%3D%22iowait%22%7D&g0.tab=1&g1.range_input=1h&g1.expr=100-avg(irate(node_cpu_seconds_total%7Bmode%3D%22idle%22%7D%5B5m%5D))without(cpu)*100&g1.tab=1&g2.range_input=1h&g2.expr=node_filesystem_avail_bytes%7Bmountpoint%3D%22%2F%22%2Cfstype!%3D%22tmpfs%22%7D%2Fnode_filesystem_size_bytes*100&g2.tab=1&g3.range_input=1h&g3.expr=node_load15&g3.tab=1)
### Задание 2
#### Установка и настройка
1. Перейти в каталог c ansible скриптами ```cd /vagrant/ansible```
2. Запустить установку графаны и окружения ```ansible-playbook provision.yml```. **Внимание** - повторная установка удалит ваши дашборы
#### Запуск сервера 
```sudo systemctl start grafana-server```
Дашбоард выложен в json-файл Task Skillbox-2 grafana.json
